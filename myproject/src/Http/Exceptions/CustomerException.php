<?php

namespace Http\Exceptions;

/**
 * Usage: Throw CustomerExceptions with messages that should be exposed to the API user.
 * @see \Config::exceptionHandler()
 * @todo move this namespace to the same level as Http
 */
class CustomerException extends \Exception
{
    public function echoMessage($msg): void
    {
        if (is_iterable($msg) || is_object($msg)) {
            if (is_array($msg)) {
                $msg = json_encode($msg);
            } else {
                $msg = "There was an error. ";
            }
        }
        echo $msg;
    }
}
