<?php

require __DIR__ . '/vendor/autoload.php'; // Load all Composer files and this app's class files (defined in composer.json)
(Dotenv\Dotenv::createImmutable(__DIR__))->load(); // Register key-value pairs from .env file so they can be loaded with getenv()
set_exception_handler(['Config','exceptionHandler']);
register_shutdown_function(['Config','shutdownHandler']);
if(0 === strcasecmp('true', getenv('PROD'))) {
    ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
}
