<?php

namespace Http;

use Http\Exceptions\CustomerException;

class Router
{
    public $request; /* @var IRequest */
    private $supportedHttpMethods = ["GET","POST"];

    public function __construct(IRequest $request)
    {
        $this->request = $request;
    }

    /**
    * Adds get() or post() routes to the get() or post() array
    * @params $httpMethod name of the called method - get() or post()
    * @params $args Closure to associate with the $httpMethod
    */
    public function __call($httpMethod, $args)
    {
        list($route, $closure) = $args;
        $parsedRoute = new Route($route, $closure);
        $this->{$httpMethod}[$parsedRoute->name] = $parsedRoute;
    }

    private function invalidMethodHandler($method): void
    {
        $msg = $this->request->serverProtocol . " 405 Method Not Allowed ".$method;
        $this->badRequest($msg, 405);
        //@codeCoverageIgnoreStart
    }
    //@codeCoverageIgnoreEnd

    private function defaultRequestHandler(string $route): void
    {
        $msg = $this->request->serverProtocol . " 404 Page Not Found " . $this->request->requestMethod . ' ' . $route;
        $this->badRequest($msg, 404);
        //@codeCoverageIgnoreStart
    }
    //@codeCoverageIgnoreEnd

    private function invalidParamsHandler(int $expected, int $actual)
    {
        $msg = $this->request->serverProtocol . " 400 Bad Request - expected $expected parameters and received $actual ";
        $this->badRequest($msg, 400);
        //@codeCoverageIgnoreStart
    }
    //@codeCoverageIgnoreEnd

    private function badRequest(string $msg, int $httpCode)
    {
        if (!headers_sent()) {
            header($msg, $httpCode);
        }
        throw new CustomerException($msg);
    }

    /**
    * Resolves the route
    */
    public function resolve()
    {
        // See if the http verb is allowed
        if (!in_array(strtoupper($this->request->requestMethod), $this->supportedHttpMethods)) {
            $this->invalidMethodHandler($this->request->requestMethod);
        } else {
            // See if a route exists for the requested http verb
            $httpMethod = strtolower($this->request->requestMethod);
            $route = $this->$httpMethod[$this->request->route] ?? null;
            $action = $this->findActionForRoute($route);
            if (is_null($action)) {
                $this->defaultRequestHandler($this->request->route);
            }
            // Call the route method and pass the Request
            $response = call_user_func_array($action, [$this->request]);
        }
    }

    /**
    * Return the function to run for the requested route and params
    * @return mixed null or \Closure
    */
    private function findActionForRoute(Route $route = null)
    {
        $closure = null;
        if (is_a($route, 'Http\Route')) {
            if ($route->numParams === count($this->request->routeParams)) {
                $closure = $route->action;
            } else {
                $this->invalidParamsHandler($route->numParams, count($this->request->routeParams));
            }
        }
        return $closure;
    }
}
