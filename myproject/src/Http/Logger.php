<?php

namespace Http;

/**
* This class extends vendor/psr/log/Psr/Log/AbstractLogger.php so we can use logger methods
* like $logger->notice() and $logger->info() without writing a bunch of boilerplate code to define them.
* The AbstractLogger methods are debug(), info(), notice(), warning(), error(), critical(), alert(), emergency()
* @ref https://www.php-fig.org/psr/psr-3/
*
* This class trackes log messages in an array. It will not write duplicate messages to the log file (regardless of timestamp),
* and it will not write more than 1,000 messages to the log file. This behavior is controlled by the writeToLogFile() and checkMessage() methods.
*
* @todo customize log behavior for environment (prod/dev; customer/developer); set up logrotate
* @todo move Logger outside of the Http namespace (normally it's in the business code, but it doesn't belong there either)
*/
class Logger extends \Psr\Log\AbstractLogger
{
    protected static $logFile = null; /* @var string */
    protected static $enabled = true; /* @var Toggle writeToLogFile() */
    protected static $disabledLevels = []; /* @var array Log levels in this array will not be written to log file by writeToLogfile() */
    private static $defaultLogFile = null;

    public function __construct($defaultLogFile = null)
    {
        $tz = date_default_timezone_get();
        date_default_timezone_set(getenv('DEFAULT_TIMEZONE'));
        self::$defaultLogFile = $defaultLogFile ?: __DIR__ . '/../../logs/app_'.date('Y-m-d').'.log';
        date_default_timezone_set($tz);
    }

    /**
    * Defines and executes our custom log behavior
    * @param mixed $level Usually a \Psr\Log\LogLevel object (that's what AbstractLogger passes)
    * @param string $message
    * @param array $context Arbitrary data. If the array includes an Exception object, it MUST have a key named Exception
    */
    public function log($level, $message, array $context = [])
    {
        if (is_array($message) || is_object($message)) {
            $message = print_r($message, 1);
        } elseif (!is_string($message)) {
            $message = (string) $message;
        }
        switch ($level) {
            case 'debug':
            case 'info':
            case 'notice':
                if (self::devMode()) {
                    $this->writeToLogFile((string) $level, $message, $context);
                }
                break;
            case 'warning':
                $this->writeToLogFile((string) $level, $message, $context);
                break;
            case 'error':
            case 'alert':
            case 'emergency':
            case 'critical':
                $this->writeToLogFile((string) $level, $message, $context);
                if (self::devMode()) {
                    echo json_encode([/*'level'=>$level,*/(string)$level=>$message/*,$context*/]);
                } else {
                    @mail('brichards@lovebookonline.com', 'dmatch err', $message);
                }
                break;
            default:
                $this->writeToLogFile((string) $level, $message, $context);
                if (self::devMode()) {
                    echo json_encode([/*'level'=>$level,*/(string)$level=>$message/*,$context*/]);
                }
                break;
        }
    }

    /**
    * Remove $levels from self::$disabledLevels and set self::$enabled to true
    */
    public function enable($levels = null)
    {
        if (is_string($levels)) {
            if (in_array($levels, self::$disabledLevels)) {
                unset(self::$disabledLevels[array_flip(self::$disabledLevels)[$levels]]);
            }
        } elseif (is_array($levels)) {
            self::$disabledLevels = array_diff(self::$disabledLevels, $levels);
        } else {
            self::$disabledLevels = [];
        }
        self::$enabled = true;
    }

    /**
    * Add $levels to self::$disabledLevels; if null, set self::$enabled to false
    */
    public function disable($levels = null)
    {
        if (is_string($levels)) { // One level
            self::$disabledLevels[]=$levels;
        } elseif (is_array($levels)) { // Array of levels
            self::$disabledLevels = array_merge(self::$disabledLevels, $levels);
        } else { // All levels
            self::$enabled = false;
        }
    }

    public function getLogFile()
    {
        if (is_null(self::$logFile)) {
            $this->setLogFile();
        }
        return self::$logFile;
    }

    protected function setLogFile()
    {
        static $triedToCreateFile = false;
        if (is_null(self::$logFile)) {
            self::$logFile = self::$defaultLogFile;
        }
        if (!file_exists(self::$logFile) && !$triedToCreateFile) {
            $tmp = []; // Output from exec() will be stored here
            $createFile = $changePermissions = 2; // Successful exec() exit code will set these to 0
            exec('touch '.self::$logFile, $tmp, $createFile);
            exec('chmod 777 '.self::$logFile, $tmp, $changePermissions);
            if (0 !== $createFile || 0 !== $changePermissions) {
                // @codeCoverageIgnoreStart
                $this->error('Error creating logfile or setting permissions to 775 ', $tmp);
                // @codeCoverageIgnoreEnd
            }
            $triedToCreateFile = true;
        }
    }

    protected function writeToLogFile(string $level, string $message, array $context): void
    {
        if (!self::$enabled || in_array($level, self::$disabledLevels)) {
            return;
        }
        if (is_null(self::$logFile)) {
            $this->setLogFile();
        }
        $message = strtoupper($level).': '.$message;// . (!empty($context) ? PHP_EOL . '\t\t' . substr(print_r($context, 1), 256) : '');
        if (isset($context['Exception']) && is_object($context['Exception']) &&  property_exists($context['Exception'], 'file') && property_exists($context['Exception'], 'line')) {
            $message .= " in ".basename($context['Exception']->getFile())." at line {$context['Exception']->getLine()}";
            $message .= " ".$context['Exception']->getMessage();
        }
        if ($this->checkMessage($message)) {
            // Only write the $message if it returns true from checkMessage()
            $tz = date_default_timezone_get();
            date_default_timezone_set(getenv('DEFAULT_TIMEZONE'));
            @file_put_contents(self::$logFile, date("Y-m-d H:i:s e").': '.$message.PHP_EOL, FILE_APPEND);
            date_default_timezone_set($tz);
        }
    }

    /**
    * Returns true if $message hasn't been logged before and there are < 1000 items in $messages cache
    */
    protected function checkMessage($message)
    {
        static $messages = [];
        if (count($messages) < 1000) { // If more than this many total errors in log, just return false
            if (!in_array($message, $messages)) {
                $messages[]=$message; // If we haven't already logged this message, return true.
                return true;
            } else {
                return false;
            }
        } else {
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }
    }

    protected static function devMode(): bool
    {
        return (0 === strcasecmp('true', getenv('PROD')));
    }
}
