<?php

use PHPUnit\Framework\TestCase;
use Http\Request;
use Http\CustomerException;

final class RequestTest extends TestCase
{
    use \HelperTrait;

    public function testSetRouteAndParams()
    {
        $request = $this->mockObject('\Http\Request');
        $route = '/this';
        $request->requestUri = "$route/is/a/route";
        $this->invokeMethod($request, 'setRouteAndParams');
        $this->assertEquals($route, $request->route, "\$request->route is not $route");
        $this->assertEquals('is', $request->routeParams[0], "\$request->routeParams[1] is not 'is'");
        $this->assertEquals('a', $request->routeParams[1], "\$request->routeParams[2] is not 'a'");
        $this->assertEquals('route', $request->routeParams[2], "\$request->routeParams[3] is not 'route'");
    }
}
