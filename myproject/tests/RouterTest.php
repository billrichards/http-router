<?php

use PHPUnit\Framework\TestCase;

use Http\CustomerException;
use Http\Router;
use Http\Route;
use Http\Request;

final class RouterTest extends TestCase
{
    use \HelperTrait;

    protected $router; /** @var Router */

    public function setUp(): void
    {
        $serverVariables = [
            'SERVER_PROTOCOL'=> 'HTTP/1.1'
        ];
        $this->request = new Request($serverVariables, '{}');
    }

    public function testGetAndPostAreSupportedHttpMethods()
    {
        $reflectionClass = new \ReflectionClass('Http\Router');
        $props = $reflectionClass->getDefaultProperties();
        $supportedHttpMethods = isset($props['supportedHttpMethods']) ? $props['supportedHttpMethods'] : [];
        $this->assertTrue(is_array($supportedHttpMethods), 'supportedHttpMethods is not an array: '.gettype($supportedHttpMethods));
        $this->assertTrue(in_array('GET', $supportedHttpMethods), 'GET is not supported '.implode($supportedHttpMethods, ', '));
        $this->assertTrue(in_array('POST', $supportedHttpMethods), 'POST is not supported '.implode($supportedHttpMethods, ', '));
    }

    public function testGetRouteResolvesSuccessfully()
    {
        $serverVariables = [
            'SERVER_PROTOCOL'=> 'HTTP/1.1',
            'REQUEST_URI'=> '/testpath',
            'REQUEST_METHOD'=>'GET'
        ];
        $request = new Request($serverVariables, '{}');
        $router = new Router($request);
        $successMessage = 'This route was successfully resolved';
        $router->get('/testpath', function (Request $request) use ($successMessage) {
            echo $successMessage;
        });
        $output = '';
        $level = ob_get_level();
        ob_start();
        $router->resolve();
        $output = ob_get_contents();
        while (ob_get_level() > $level) {
            ob_end_clean();
        }
        $this->assertEquals($successMessage, $output);
    }

    public function testPostRouteResolvesSuccessfully()
    {
        $serverVariables = [
            'SERVER_PROTOCOL'=> 'HTTP/1.1',
            'REQUEST_URI'=> '/testpath',
            'REQUEST_METHOD'=>'POST'
        ];
        $successMessage = 'this is the output from testing the post route';
        $body = ['content'=>$successMessage];
        $request = new Request($serverVariables, json_encode($body));
        $router = new Router($request);
        $router->post('/testpath', function (Request $request) {
            echo $request->getBody()['content'];
        });
        $output = '';
        $level = ob_get_level();
        ob_start();
        $router->resolve();
        $output = ob_get_contents();
        while (ob_get_level() > $level) {
            ob_end_clean();
        }
        $this->assertEquals($successMessage, $output);
    }

    /**
    * @param string $exceptionErrorText Error message passed by reference and overwritten by this method
    * @param bool $customerExceptionCaught Pass-by-reference so that another method can call this multiple times
    */
    private function resolveAndBufferOutput(string &$exceptionErrorText, bool &$customerExceptionCaught): string
    {
        $customerExceptionCaught = false;
        $exceptionErrorText = '';
        $level = ob_get_level();
        ob_start();
        try {
            (new Router($this->request))->resolve();
        } catch (CustomerException $e) {
            $customerExceptionCaught= true;
            \Config::exceptionHandler($e);
        } catch (\Exception $e) {
            $exceptionErrorText = ' '.get_class($e).' was caught instead. ';
        }
        $output = ob_get_contents();
        while (ob_get_level() > $level) {
            ob_end_clean();
        }
        return (string) $output;
    }
}
