<?php

use PHPUnit\Framework\TestCase;
use Http\Route;

final class RouteTest extends TestCase
{
    public function testRoute()
    {
        $r = new Route('/', 'action');
        $this->assertSame($r->numParams, 0, "Route has {$r->numParams} params");
    }

    public function testRouteWithOneParam()
    {
        $path = '/hellomynameisroute';
        $action = 'party';
        $r = new Route($path, $action);
        $this->assertSame($r->numParams, 0, "Route has {$r->numParams} params");
        $this->assertSame($r->name, $path, "Route is named {$r->name} not $path");
        $this->assertSame($r->action, $action, "Route action is {$r->action} not $action");
    }

    public function testRouteWithFiveParams()
    {
        $route = '/hello';
        $path = "$route/my/name/is/route";
        $action = 'fiesta';
        $r = new Route($path, $action);
        $this->assertSame($r->numParams, 4, "Route has {$r->numParams} params");
        $this->assertSame($r->name, $route, "Route is named {$r->name} not $route");
        $this->assertSame($r->action, $action, "Route action is {$r->action} not $action");
    }
}
