<?php
use \Http\Router;
use \Http\Request;

################################################
# Register autoloader, shutdown function, and exception handler
################################################
require __DIR__ .'/../app.php'; // Loads required files

################################################
# Initialize the router and define our routes
################################################
$router = new Router(new Request($_SERVER));

$router->get('/', function(Request $request) {
    echo 'Welcome';
});

$router->get('/get/{id}', function(Request $request) {
    echo "Run the business code to retrive object by ID";
});

$router->post('/chooseVendorAndShipping', function(Request $request) {
    echo "Run the business code to perform the operation on the POST data: ".json_encode($_POST, JSON_PRETTY_PRINT);
});

$router->get('/report', function(Request $request) {
    echo "Run the business code to generate the report";
});

################################################
# Resolve the route and exit
################################################
$router->resolve();
exit();
