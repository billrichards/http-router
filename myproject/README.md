# MyProject

Routes HTTP requests. Bring your own business code.

## Prerequisites

#### Required

PHP 7.3+, composer, MySQL 5.6+, curl, a web server (e.g Apache or nginx)

#### Recommended

Apache 2.4

## Installing

Clone the project and run `composer install`.  On production systems, run `composer install --no-dev`.

#### Configure the .env file

In the root of the project, copy the included `.env.example` file to `.env` and update all of the values for this install.

#### Configure Apache

All requests to this api must be routed by the web server to `public/index.php`.  The `.htaccess` file includes Apache rewrite rules, as well as instructions for configuring the `<VirtualHost>` entry in the system's httpd configuration file.

#### Configure log directory

Make sure the `logs` directory at project root is writable by all users who will execute the app, for example `apache` and `root`.  On dev systems this might include the local user as well.

## Usage

* Rename the src/MyProject directory for your project, and create your business code there.
* Update composer.json with the proper namespace for your business code.
* Add your entry points in src/public/index.php
* Add your tests in src/tests
