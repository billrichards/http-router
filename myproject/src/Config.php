<?php

class Config
{
    /**
    * Returns a PDO database connection
    * @return \PDO
    */
    public static function getPdoConnection()
    {
        static $pdo = null;
        $connected = function ($pdo) {
            try {
                @$pdo->query('select 1');
                return true;
            } catch (\PDOException $e) {
                return false; // Not connected
            }
        };
        if (null === $pdo || !$connected($pdo)) {
            $host = getenv('MYSQL_HOST'); // defined in .env file
            $db= getenv('MYSQL_DB');
            $user = getenv('MYSQL_USER');
            $pass = getenv('MYSQL_PASS');

            $dsn = "mysql:host=$host;dbname=$db;charset=utf8mb4";

            $options = [
                \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_LAZY, // maybe just do FETCH_ASSOC https://phpdelusions.net/pdo/fetch_modes#FETCH_LAZY
                \PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            try {
                $pdo = @new \PDO($dsn, $user, $pass, $options);
            } catch (\PDOException $e) {
                static::logError($e->getMessage(), 'critical', ['Exception'=>$e,'Backtrace'=>debug_backtrace()]);
            }
        }
        return $pdo;
    }

    /**
    * Calls static::logError() if error_get_last() a Warning or Fatal.
    */
    public static function shutdownHandler()
    {
        $err = error_get_last();
        if (is_array($err)) {
            if (static::isFatal($err['type'])) {
                $msg = 'Fatal error in '.basename($err['file']) . ' at line ' . $err['line'].': '.$err['message'];
                $method = 'critical';
            } elseif (static::isWarning($err['type'])) {
                $msg = 'PHP warning in '.basename($err['file']) . ' at line ' . $err['line'].': '.$err['message'];
                $method = 'error';
            }
            if (isset($method)) {
                static::logError($msg, $method, $err, 'A fatal error has occurred');
            }
        }
    }

    /**
    * Returns true if $errorCode indicates a Fatal error
    * @ref http://php.net/manual/en/errorfunc.constants.php
    */
    private static function isFatal($errorCode): bool
    {
        return in_array($errorCode, [1, 16, 64, 256, 4096]);
    }

    /**
    * Returns true if $errorCode indicates a Warning error
    * @ref http://php.net/manual/en/errorfunc.constants.php
    */
    private static function isWarning($errorCode): bool
    {
        return in_array($errorCode, [2, 32, 128, 512, 2048, 8192]);
    }

    private static function logError(string $msg, string $loggerMethod = 'critical', array $err = [], string $defaultMessage = 'error'): void
    {
        if (class_exists('\Http\Logger') && method_exists('\Http\Logger', $loggerMethod)) {
            (new \Http\Logger())->$loggerMethod($msg, $err);
        } elseif (isset($loggerMethod) && 'critical' === $loggerMethod) {
            echo json_encode(['Error:'=>$defaultMessage]);
        }
    }

    /**
    * Custom exception handler for uncaught or thrown exceptions.
    */
    public static function exceptionHandler($e)
    {
        $dbt=debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
        $caller = isset($dbt[1]['file']) ? $dbt[1]['file'] : '';
        $caller .= isset($dbt[1]['line']) ? ':'.$dbt[1]['line'].' ' : '';
        switch (@get_class($e)) {
            case '\Http\Exceptions\CustomerException':
            case 'Http\Exceptions\CustomerException':
            case 'CustomerException':
                static::logError($caller.$e->getMessage(), 'info');
                $e->echoMessage($e->getMessage()); // Call CustomerException's echoMessage() method
                break;
            default:
                if (is_object($e) && method_exists($e, 'getMessage')) {
                    $msg = $e->getMessage();
                    $array = ['Exception'=>$e];
                } else {
                    $msg = substr(var_export($e, 1), 0, 100);
                    $array = ['Backtrace'=>$dbt];
                }
                static::logError($msg, 'critical', $array, 'An exception has occurred');
                break;
        }
    }
}
