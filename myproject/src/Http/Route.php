<?php

namespace Http;

class Route
{
    public $action;
    public $numParams;
    public $name;

    /**
    * @param string $route the requested route including {params}
    * @param mixed $action Function or method to run for this route (typically a closure from index.php)
    */
    public function __construct(string $route, $action)
    {
        if (false === strstr(ltrim($route, '/'), '/')) {
            $this->numParams = 0;
            $this->name = $route;
        } else {
            $p = explode('/', ltrim($route, '/'));
            $this->numParams = count($p) - 1;
            $this->name = '/'.$p[0];
        }
        $this->action = $action;
    }
}
