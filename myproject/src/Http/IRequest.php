<?php

namespace Http;

interface IRequest
{
    public function getBody();
}
