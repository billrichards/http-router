<?php

use Http\Request;

trait HelperTrait
{
    /**
    * Returns an object without running its constructor
    */
    public function mockObject(string $namespaceAndClassname)
    {
        return (new \ReflectionClass($namespaceAndClassname))->newInstanceWithoutConstructor();
    }

    /**
    * Use ReflectionClass to call protected/private method of a class.
    *
    * @param object &$object    Instantiated object that we will run method on.
    * @param string $methodName Method name to call
    * @param array  $parameters Array of parameters to pass into method.
    *
    * @return mixed Method return
    * @ref https://jtreminio.com/blog/unit-testing-tutorial-part-iii-testing-protected-private-methods-coverage-reports-and-crap/
    */
    public function invokeMethod(object &$object, string $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

}
