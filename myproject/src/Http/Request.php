<?php

namespace Http;

class Request implements IRequest
{
    public $requestUri; /* @var string */
    public $route; /* @var string The first item in requestUri */
    public $routeParams; /* @var array Everything after the first item in requestUri becomes the $routeParams array */
    public $serverProtocol;
    public $requestMethod;
    private $propertiesFromServer = ['requestUri','route','serverProtocol','requestMethod'];
    private $body;

    /**
    * @param array $server $_SERVER superglobal or similar data
    * @param string $json Optional json string (defaults to php://input if null)
    */
    public function __construct(array $server, string $json = null)
    {
        foreach ($server as $k => $v) {
            $propName = $this->toCamelCase($k);
            if (property_exists($this, $propName) && in_array($propName, $this->propertiesFromServer)) {
                $this->$propName = $v;
            }
        }
        $this->setRouteAndParams();
        $this->setBody($json);
    }

    protected function validate()
    {
        //@codeCoverageIgnoreStart
    }
    //@codeCoverageIgnoreEnd

    /**
    * Converts $string to camelCase. SERVER_PROTOCOL becomes serverProtocol, etc
    * @param string
    * @return string
    */
    private function toCamelCase(string $string): string
    {
        $string = strtolower($string);
        preg_match_all('/_[a-z]/', $string, $matches);
        foreach ($matches[0] as $match) {
            $c = str_replace('_', '', strtoupper($match));
            $string = str_replace($match, $c, $string);
        }
        return $string;
    }

    /**
    * Returns key-value pairs of the request body
    */
    public function getBody()
    {
        return $this->body;
    }

    protected function setBody($json)
    {
        $body =null;
        if ($this->requestMethod == "POST") {
            $body = [];
            $json = $json ?: file_get_contents('php://input');
            $raw = json_decode($json, 1);
            if (!is_array($raw)) {
                $this->validationErrors[]='Malformed request body';
            } else {
                foreach ($raw as $key => $value) {
                    $body[$key] = $value;
                }
            }
        }
        $this->body = $body;
    }

    /**
    * Sets $this->route to the first item in the URI (/create/ or just /)
    * Sets $this->routeParams array to /any/subsequent/items in the uri
    */
    private function setRouteAndParams()
    {
        $route = rtrim($this->requestUri, '/');
        if ($route === '') {
            $route = '/';
        } elseif (stristr(ltrim($route, '/'), "/")) {
            $route = "/".explode('/', $route)[1];
        }
        $this->route = $route;
        $params = array_filter(explode('/', str_replace($this->route, '', $this->requestUri)), function ($row) {
            return !empty($row);
        });
        $this->routeParams = array_values($params);
    }
}
